from db_using_xml import Xml_File

def get_XmlDbFile_object():
    '''Helper function to repeatedly query the user for a valid file name'''

    my_first_object = None
    while (1):
        user_input = input("Enter the name of the table/file you want to connect to:")
        try:
            my_first_object = Xml_File(user_input)
            my_first_object.student_menu()
            # print(user_input + Xml_File.file_suffix, "created in ./data folder.")
            break
        except TypeError:
            print("Please enter a valid type for user input. \n Try again")

        except ValueError:
            print("The table name you have entered needs to be alphabetic of at least length 1. \n Try again")

    return my_first_object
get_XmlDbFile_object()
