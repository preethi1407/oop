import os
import uuid
# import xmltodict
import xml.etree.cElementTree as ET
class Xml_File:
    ''' class representing a xml file '''
    directory_prefix = "data/"
    file_suffix = ".xml"

    def __init__(self,file_name):
        '''This is a constructor which accepts a file_name as the parameter'''
        
        # checking if the type of file_name is string or else throwing a type error
        if type(file_name) != str:
            raise TypeError("Expected string argument but received ",type(file_name))

        # checking if the file_name is an alphabetical string
        if not file_name.isalpha():
            raise ValueError("Expected alphabetical string but received ",type(file_name))

        # obtaining the path of the current directory
        file_dir = os.path.dirname(os.path.realpath('__file__'))

        # obtaining the absolute path of the data file
        self.file_name = os.path.join(file_dir, file_name + self.file_suffix)
        
        if not os.path.exists(self.file_name):
            print(file_name,"file does not exist")
            print("creating a new file")
            f = open(self.file_name, "x")

    def read_data(self):
        '''Helper method to read the data from the xml file'''

        tree = ET.parse('employees.xml')
        root = tree.getroot()
        for i in root.findall('students'):
            student_id=i.find('student_id')
            id = student_id.text if student_id is not None else None
            student_name=i.find('student_name')
            name = student_name.text if student_name is not None else None
            student_branch=i.find('branch')
            branch = student_branch.text if student_branch is not None else None
            student_year=i.find('year')
            year = student_year.text if student_year is not None else None
            student_email=i.find('email')
            email = student_email.text if student_email is not None else None
            student_contact=i.find('contact')
            contact = student_contact.text if student_contact is not None else None
            print("student id: ",id)
            print("student name: ",name)
            print("branch: ",branch)
            print("year of study: ",year)
            print("email id: ",email)
            print("contact number: ",contact)
            
    def read_data_by_id(self):
        '''Helper method to read the data of the specified id'''
        user_id = input("Enter the id of the student you want to search for")
        tree = ET.parse('employees.xml')
        root = tree.getroot()
        for i in root.findall('students'):
            student_id=i.find('student_id')
            id = student_id.text if student_id is not None else None
            if user_id == id:
                student_name=i.find('student_name')
                name = student_name.text if student_name is not None else None
                student_branch=i.find('branch')
                branch = student_branch.text if student_branch is not None else None
                student_year=i.find('year')
                year = student_year.text if student_year is not None else None
                student_email=i.find('email')
                email = student_email.text if student_email is not None else None
                student_contact=i.find('contact')
                contact = student_contact.text if student_contact is not None else None
                print("student id: ",id)
                print("student name: ",name)
                print("branch: ",branch)
                print("year of study: ",year)
                print("email id: ",email)
                print("contact number: ",contact)       

    def write_data(self):
        '''Helper method to write the data into an xml file
        Parameters:
            dict_input : should be a valid dictionary
        '''
        user_id=input("enter the id of the student you want to create")
        tree = ET.parse('employees.xml')
        root = tree.getroot()
        for i in root.findall('students'):
            student_id=i.find('student_id')
            id = student_id.text if student_id is not None else None
            if user_id == id:
                print("student already exists")
                break
        kotta_id=id
        if(user_id!=kotta_id):
            print("creating a new profile for the student")
            new_id = uuid.uuid1()
            student_name=input("Enter the student Name:")
            student_branch=input("enter the branch of the student")
            student_year=input("enter the year of study of the student")
            student_email=input("enter the email id of the student")
            student_contact=input("enter the contact number of the student")    
            def get_new_element():
                students = ET.Element('students')
                id = ET.SubElement(students, 'student_id')
                id.text=str(new_id)
                name = ET.SubElement(students, 'student_name')
                name.text=student_name
                branch = ET.SubElement(students, 'branch')
                branch.text=student_branch
                year = ET.SubElement(students, 'branch')
                year.text=student_year
                mail_id = ET.SubElement(students, 'email')
                mail_id.text=student_email
                contact = ET.SubElement(students, 'contact')
                contact.text=student_contact

                return students
            # if __name__ == '__main__':
            xml_tree = ET.parse('employees.xml')
            student_element = xml_tree.find('students')

            new_element = get_new_element()
            student_element.append(new_element)

            new_xml_tree_string = ET.tostring(xml_tree.getroot())

            with open('new.xml', "wb") as f:
                f.write(new_xml_tree_string)
        print("new profile successfully created at 'new.xml'")

    def del_data(self):
    #     '''Helper method to delete data from an xml file '''
        ntree = ET.parse('employees.xml')
        root = ntree.getroot()
		
		#get a variable for the record that you want to delete
        deleterecord=input("Enter the id of the student record you want to delete: ")


		#OK, now loop through all records looking for specified email address
        for i in root.findall('students'):
			#get the email of the current record, 
            student_id= i.find('student_id')
            id = student_id.text if student_id is not None else None
			#check is this the email we are looking for?
            if id == deleterecord:
				#YES: remove the record from the xml tree
                root.remove(i)   
                new_xml_tree_string = ET.tostring(ntree.getroot())

                with open('deleted_students.xml', "wb") as f:
                    f.write(new_xml_tree_string) 
                print("student profile deleted successfully @deleted_students.xml")
            kotta_id=id
            break
        if(kotta_id!=deleterecord):
            print("student does not exist")        
				


    # def del_data_by_id(self):
    #     '''Helper method to delete the data of the specified id
    #     Parameters:
    #         id:should be a valid UUID '''

        

    def update_data(self):
    

        '''Helper method to write the new data into a XML file
        Parameters:
            id: should be a valid UUID
            new_data: should be a valid dictionary'''
        ntree = ET.parse('employees.xml')
        root = ntree.getroot()
        user_id=input("enter the id of the student you want to update")
        for i in root.findall('students'):
            student_id= i.find('student_id')
            id = student_id.text if student_id is not None else None          
            print("enter the field you want to modify \n 1: student_id \n 2:student_name \n 3:branch \n 4:year \n 5:email \n 6:contact number")
            choice=int(input())
            if choice==1:
                
                new_id=input("enter the new student id ")
                for id in root.iter('student_id'):
        
                    id.text = new_id
                    id.set('newid', 'yes')
                    new_xml_tree_string = ET.tostring(ntree.getroot())

                    with open('updated_students.xml', "wb") as f:
                        f.write(new_xml_tree_string) 
                print("student profile updated successfully @updated_students.xml")
                break

            elif(choice==2):
                new_name=input("enter the new student name")
                for name in root.iter('student_name'):
        
                    name.text = new_name
                    name.set('new', 'yes')
                    new_xml_tree_string = ET.tostring(ntree.getroot())

                    with open('updated_students.xml', "wb") as f:
                        f.write(new_xml_tree_string) 
                print("student profile updated successfully @updated_students.xml")
                break
            elif(choice==3):
                new_name=input("enter the new branch name")
                for branch in root.iter('branch'):
        
                    branch.text = new_name
                    branch.set('new', 'yes')
                    new_xml_tree_string = ET.tostring(ntree.getroot())

                    with open('updated_students.xml', "wb") as f:
                        f.write(new_xml_tree_string) 
                print("student profile updated successfully @updated_students.xml")
                break
            elif(choice==4):
                new_name=input("enter the new value of year")
                for year in root.iter('year'):
        
                    year.text = new_name
                    year.set('new', 'yes')
                    new_xml_tree_string = ET.tostring(ntree.getroot())

                    with open('updated_students.xml', "wb") as f:
                        f.write(new_xml_tree_string) 
                print("student profile updated successfully @updated_students.xml")
                break
            elif(choice==5):
                new_name=input("enter the new email id")
                for email in root.iter('email'):
        
                    email.text = new_name
                    email.set('new', 'yes')
                    new_xml_tree_string = ET.tostring(ntree.getroot())

                    with open('updated_students.xml', "wb") as f:
                        f.write(new_xml_tree_string) 
                print("student profile updated successfully @updated_students.xml")
                break       
            else:
                new_name=input("enter the new contact number")
                for contact in root.iter('contact'):
        
                    contact.text = new_name
                    contact.set('new', 'yes')
                    new_xml_tree_string = ET.tostring(ntree.getroot())

                    with open('updated_students.xml', "wb") as f:
                        f.write(new_xml_tree_string) 
                print("student profile updated successfully @updated_students.xml")
                break
    
    def student_menu(self):
        print("")
        print("1 -Show details of all students")
        print("2 -show details of a specific student")
        print("3 -Create a new student profile")
        print("4 -delete the profile of a specific student")
        print("5 -update the profile of a specific student")
        user_input=input("select your choice ")

        while user_input !="6":

            if user_input =="1":
                self.read_data()
                break

            if user_input =="2":
                self.read_data_by_id()
                break

            if user_input =="3":
                self.write_data()
                break

            if user_input =="4":
                self.del_data()
                break

            if user_input =="5":
                self.update_data()
                break

    def __del__(self):
        print("destroying the object")
